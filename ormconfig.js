const env = require('dotenv');
env.config();

console.log('env', process.env.DATABASE_URL);

const isProd = process.env.NODE_ENV === 'production';
const entitiesExtension = isProd ? 'js' : 'ts';
const entitiesDir = isProd ? 'dist' : 'src';
const migrationsDir = isProd ? 'dist/migration/*.js' : 'migration/*.ts';

module.exports = {
  type: 'postgres',
  entities: [
    `${__dirname}/${entitiesDir}/**/*.entity.${entitiesExtension}`,
    `${__dirname}/${entitiesDir}/**/entity/*.${entitiesExtension}`,
  ],
  url: process.env.DATABASE_URL || 'postgres://postgres:2732415@127.0.0.1:5432/bwriter',
  migrations: [migrationsDir],
  cli: {
    migrationsDir: 'migration',
  },
};
