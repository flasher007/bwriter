FROM node:10.15.3
RUN npm install -g yarn node-gyp typescript ts-node 
WORKDIR /app
COPY . .
RUN yarn install
EXPOSE 4000
CMD [ "yarn", "run", "start:prod"]