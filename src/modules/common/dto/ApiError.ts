import { ApiResponseModelProperty } from '@nestjs/swagger';

export class ApiError {
  @ApiResponseModelProperty()
  public readonly statusCode: number;

  @ApiResponseModelProperty()
  public readonly error: string;

  @ApiResponseModelProperty()
  public readonly message: string;
}
