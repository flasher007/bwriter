import { Module } from '@nestjs/common';
import { DocumentsController } from './DocumentsController';
import { DocumentsService } from './DocumentsService';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentRepository } from './repository/DocumentRepository';

@Module({
  imports: [TypeOrmModule.forFeature([DocumentRepository])],
  controllers: [DocumentsController],
  providers: [DocumentsService],
})
export class DocumentsModule {}
