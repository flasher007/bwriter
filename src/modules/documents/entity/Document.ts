import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity({ name: 'documents' })
export class Document {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  documentId: string;

  @Column({ nullable: false, type: 'json' })
  rawData: object;

  @Column({ nullable: true })
  prevHash: string;

  @Column({ nullable: true })
  txHash: string;

  @Column({ nullable: true, type: 'timestamp with time zone' })
  txDate: string;

  @Column({ default: false })
  txStatus: boolean;

  @Column({ default: 'processing' })
  status: string;

  @Column({ nullable: true })
  statusText: string;

  @Column({ nullable: true, type: 'json' })
  writeData: object;

  @Column({ nullable: false, type: 'timestamp with time zone' })
  date: string;

  @CreateDateColumn({ type: 'timestamp with time zone' })
  createdAt: string;

  @UpdateDateColumn({ type: 'timestamp with time zone' })
  updatedAt: string;
}
