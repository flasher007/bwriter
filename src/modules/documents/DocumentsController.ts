import { Controller, Post, HttpStatus, Body, BadRequestException, Get, Param, NotFoundException } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiImplicitParam } from '@nestjs/swagger';
import { DocumentsService } from './DocumentsService';
import { Document } from './dto/Document';
import { DocumentRequest } from './dto/DocumentRequest';

@ApiUseTags('Document')
@Controller('documents')
export class DocumentsController {
  constructor(private readonly service: DocumentsService) {}

  @Post('/')
  @ApiResponse({ status: HttpStatus.OK, type: Document })
  async addData(@Body() data: DocumentRequest): Promise<Document> {
    const { documentId, rawData } = data;
    const exist = await this.service.findByDocumentId(documentId);

    if (exist) {
      throw new BadRequestException('Document already exists');
    }

    const document = await this.service.create(documentId, rawData);

    return document;
  }

  @ApiImplicitParam({ name: 'id', description: 'Document id', type: String })
  @Get('/:id')
  async get(
    @Param('id') id: string,
  ): Promise<Document> {
    const document = await this.service.findByDocumentId(id);

    if (!document) {
      throw new NotFoundException('Brief not found');
    }

    return document;
  }
}
