import { EntityRepository, Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { Document } from '../entity/Document';

@Injectable()
@EntityRepository(Document)
export class DocumentRepository extends Repository<Document> {}
