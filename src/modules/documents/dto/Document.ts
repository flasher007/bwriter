import { IsEmail, IsNotEmpty, MinLength, MaxLength, IsJSON } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class Document {
  @ApiModelProperty()
  readonly id: number;

  @ApiModelProperty()
  readonly documentId: string;

  @ApiModelProperty()
  readonly writeData?: object;

  @ApiModelProperty()
  readonly txHash?: string;

  @ApiModelProperty({nullable: true})
  readonly txDate?: string;

  @ApiModelProperty()
  readonly date?: string;

  @ApiModelProperty()
  readonly prevHash: string;

  @ApiModelProperty()
  readonly createdAt: string;
}
