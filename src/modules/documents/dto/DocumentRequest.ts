import { IsJSON } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class DocumentRequest {
  @ApiModelProperty({ required: true })
  @IsJSON()
  readonly rawData: 'json';

  @ApiModelProperty({ required: true })
  readonly documentId: string;
}
