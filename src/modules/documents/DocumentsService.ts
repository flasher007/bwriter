import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Document } from './dto/Document';
import {
  transfer,
  data,
  broadcast,
  IDataParams,
} from '@acryl/acryl-transactions';
import * as moment from 'moment';
import { DocumentRepository } from './repository/DocumentRepository';

@Injectable()
export class DocumentsService {
  constructor(private readonly repository: DocumentRepository) {}

  async create(documentId: string, rawData: string): Promise<Document> {
    const jsonData = JSON.parse(rawData);
    const prevHash = await this.lastHash();
    const date = moment(jsonData['Show date'], 'MMMM DD, YYYY HH:mm GTM');
    const insertData = {
      documentId,
      rawData: jsonData,
      prevHash,
      date: date.toString(),
    };
    let tx = null;
    let object = await this.save(insertData);

    tx = await this.sendTx(object);
    object = await this.update(object, tx);

    return object[0];
  }

  async sendTx(object: any) {
    const nodeUrl = process.env.NODE_URL;
    const seed = process.env.SEED;
    const dataParams: IDataParams = {
      data: [
        {
          key: 'data',
          value: JSON.stringify(object.rawData),
        },
        {
          key: 'previousId',
          value: object.prevHash,
        },
      ],
    };

    const signedDataTx = data(dataParams, seed);

    return await broadcast(signedDataTx, nodeUrl);
  }

  async update(obj: any, tx: any) {
    const txDate = moment(tx.timestamp);
    let result: any;

    obj.txDate = txDate.toDate();
    obj.txStatus = true;
    obj.txHash = tx.id;

    result = await this.repository.save(obj);

    return result;
  }

  async lastHash() {
    const filter: any = {
      order: {
        createdAt: 'DESC',
      },
    };

    const object = await this.repository.findOne(filter);

    return object ? object.txHash : null;
  }

  async findByDocumentId(documentId: string) {
    return await this.repository.findOne({
      where: {
        documentId,
      },
    });
  }

  async save(attribures: any) {
    const document = await this.repository.create(attribures);
    return await this.repository.save(document);
  }

  async findAll(filter: any): Promise<Document[]> {
    return await this.repository.find(filter);
  }
}
